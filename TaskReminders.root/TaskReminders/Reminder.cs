﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
public class Reminder
{
    public int Notification_Contact_Id { get; set; }
    public int? Task_Contact_Id { get; set; }
    public int? Supplier_No { get; set; }
    public string Supplier_Name { get; set; }
    public string CorrespondenceEmail { get; set; }
    public string UserName { get; set; }
    public string Firstname { get; set; }
    public string Surname { get; set; }
    public bool? branch_in { get; set; }
    public string Referee_Surname { get; set; }
    public string Referee_Other_Names { get; set; }
    public string Referee_Address1 { get; set; }
    public string Referee_Post_Outcode { get; set; }
    public string Referee_Post_Incode { get; set; }
    public DateTime Action_Date { get; set; }
    public string Task_Description { get; set; }
}
public class StatusReminder {
    public int Id { get; set; }
    public string Referee_Surname { get; set; }
    public string Referee_Other_Names { get; set; }
    public string Referee_Address1 { get; set; }
    public string Referee_Post_Outcode { get; set; }
    public string Referee_Post_Incode { get; set; }
    public DateTime Action_Date { get; set; }
    public string Firstname { get; set; }
    public string CorrespondenceEmail { get; set; }
    public string UserName { get; set; }
    public string Task_Description { get; set; }
}
public class ReminderUpdate
{
    public int Id { get; set; }
}
    public class ReminderData : DataAccessBase
{
    public List<Reminder> GetReminders()
    {
        return ProgramDb.ExecuteSprocAccessor<Reminder>("v5net_prc_get_TaskNotification_Tasks").ToList();
    }
    public List<StatusReminder> GetStatusReminders()
    {
        return ProgramDb.ExecuteSprocAccessor<StatusReminder>("v5net_prc_get_TaskNotification_Status_Tasks").ToList();
    }
    public int UpdateReminder(ReminderUpdate update)
    {
        using (var dbCommand = ProgramDb.GetStoredProcCommand("v5net_prc_Upd_Referral_Status_Task_Email_Date"))
        {
            ProgramDb.AddInParameter(dbCommand, "@Id", DbType.Int32, update.Id);
            return ProgramDb.ExecuteNonQuery(dbCommand);
        }
    }
}