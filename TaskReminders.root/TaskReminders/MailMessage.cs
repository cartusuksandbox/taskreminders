using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Transactions;
using System.Threading.Tasks;
namespace Cartus.Mailing
{
    public class MailerEmail : IDisposable
    {
        public int ID { get; set; }
        public DateTime QUEUED_DATE { get; set; }
        public string APPLICATION { get; set; }
        public bool HTML { get; set; }
        public string SUBJECT { get; set; }
        public string BODY { get; set; }
        public int STATUS { get; set; }
        public DateTime? SENT_DATE { get; set; }
        public int RETRY_COUNT { get; set; }
        public List<MailerAddress> ADDRESSES { get; set; } = new List<MailerAddress>();
        public List<MailerAttachment> ATTACHMENTS { get; set; } = new List<MailerAttachment>();
        bool _disposed;
        private string _connString = string.Empty;
        public bool Queue(string connectionString)
        {
            int EML_ID_EML;
            _connString = connectionString;
            //Validation checks for present / correct data

            if (connectionString.ToString() == string.Empty)
            {
                throw new Exception("Connection string is missing from function call");
            }
            string[] fieldsToCheck = { "APPLICATION", "SUBJECT", "BODY" };
            foreach (string field in fieldsToCheck)
            {
                dynamic val = GetType().GetProperty(field).GetValue(this);
                if (val == null || val.ToString() == string.Empty)
                {
                    logError($"Required property missing: {field}", null);
                    return false;
                }
            }
            if (ADDRESSES.Count == 0)
            {
                logError("No email addresses specificed", null);
                return false;
            }
            if (ADDRESSES.Count == 1)
            {
                logError("At least one email address of each type is required", null);
                return false;
            }
            if (ADDRESSES.FindAll(add => add.TYPE == AddressType.TO).Count < 1)
            {
                logError("You must have at least one \"To\" email address", null);
                return false;
            }
            if (ADDRESSES.FindAll(add => add.TYPE == AddressType.FROM).Count != 1)
            {
                logError("You must have exactly one \"From\" email address", null);
                return false;
            }

            //Attempt the transaction

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection conn = new SqlConnection(_connString))
                    {
                        conn.Open();
                        //Message
                        using (SqlCommand cmdMsg = new SqlCommand())
                        {
                            cmdMsg.CommandType = System.Data.CommandType.StoredProcedure;
                            cmdMsg.Connection = conn;
                            cmdMsg.CommandText = "MAILER_INS_EMAIL";
                            cmdMsg.Parameters.Add(new SqlParameter("@APPLICATION", APPLICATION));
                            cmdMsg.Parameters.Add(new SqlParameter("@HTML", HTML));
                            cmdMsg.Parameters.Add(new SqlParameter("@SUBJECT", SUBJECT));
                            cmdMsg.Parameters.Add(new SqlParameter("@BODY", BODY));
                            EML_ID_EML = (int)cmdMsg.ExecuteScalar();
                        }
                        if (EML_ID_EML == 0)
                        {
                            logError("Returned ID for email was 0", null);
                            return false;
                        }
                        //Each address
                        Parallel.ForEach(ADDRESSES, addr => {
                            using (SqlCommand cmdAddresses = new SqlCommand())
                            {
                                cmdAddresses.CommandType = System.Data.CommandType.StoredProcedure;
                                cmdAddresses.Connection = conn;
                                cmdAddresses.CommandText = "MAILER_INS_ADDRESS";
                                cmdAddresses.Parameters.Add(new SqlParameter("@EML_ID_EML", EML_ID_EML));
                                cmdAddresses.Parameters.Add(new SqlParameter("@ADDRESS", addr.ADDRESS));
                                cmdAddresses.Parameters.Add(new SqlParameter("@DISPLAY", addr.DISPLAY));
                                cmdAddresses.Parameters.Add(new SqlParameter("@TYPE", addr.TYPE));
                                cmdAddresses.Parameters.Add(new SqlParameter("@SORT_ORDER", addr.SORT_ORDER));
                                cmdAddresses.ExecuteNonQuery();
                            }
                        });
                        //Attachments
                        Parallel.ForEach(ATTACHMENTS, att =>
                        {
                            using (SqlCommand cmdAttach = new SqlCommand())
                            {
                                cmdAttach.CommandType = System.Data.CommandType.StoredProcedure;
                                cmdAttach.Connection = conn;
                                cmdAttach.CommandText = "MAILER_INS_ATTACHMENT";
                                cmdAttach.Parameters.Add(new SqlParameter("@EML_ID_EML", EML_ID_EML));
                                cmdAttach.Parameters.Add(new SqlParameter("@NAME", att.NAME));
                                cmdAttach.Parameters.Add(new SqlParameter("@DATA", att.DATA));
                                cmdAttach.Parameters.Add(new SqlParameter("@SORT_ORDER", att.SORT_ORDER));
                                cmdAttach.ExecuteNonQuery();
                            }
                        });
                        //Close connection
                        conn.Close();
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                logError($"Error during Queue(): {ex.Message}", null);
                return false;
            }

            return true;
        }
        public void logError(string errorMessage, Exception inEx)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_connString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("MAILER_INS_EXCEPTION", conn))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@APPLICATION", APPLICATION));
                        cmd.Parameters.Add(new SqlParameter("@MESSAGE", errorMessage));
                        cmd.Parameters.Add(new SqlParameter("@TRACE", inEx != null ? inEx.StackTrace : string.Empty));
                        cmd.Parameters.Add(new SqlParameter("@DATA", inEx != null ? inEx.Data.ToString() : string.Empty));
                        cmd.Parameters.Add(new SqlParameter("@SOURCE", inEx != null ? inEx.Source : string.Empty));
                        cmd.ExecuteNonQuery();
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Code to correctly implement IDispose

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        ~MailerEmail()
        {
            Dispose(false);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                // free other managed objects that implement
                // IDisposable only
            }
            // release any unmanaged objects
            // set the object references to null
            _disposed = true;
        }

    }
    public class MailerAddress
    {
        public int ID { get; set; }
        public int EML_ID_EML { get; set; }
        public string ADDRESS { get; set; }
        public string DISPLAY { get; set; } = string.Empty;
        public string TYPE { get; set; }
        public int? SORT_ORDER { get; set; } = 0;
    }
    public class MailerAttachment
    {
        public int ID { get; set; }
        public int EML_ID_EML { get; set; }
        public string NAME { get; set; }
        public byte[] DATA { get; set; }
        public int? SORT_ORDER { get; set; } = 0;
    }
    public static class AddressType
    {
        public const string TO = "TO";
        public const string FROM = "FROM";
        public const string CC = "CC";
        public const string BCC = "BCC";
    }
}