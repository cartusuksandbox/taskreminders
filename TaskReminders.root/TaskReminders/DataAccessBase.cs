﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Configuration;
    public abstract class DataAccessBase
    {
        public Database ProgramDb
        {
            get
            {
            bool IsLive = false;
            bool.TryParse(ConfigurationManager.AppSettings["IsLive"], out IsLive);
                return DatabaseFactory.CreateDatabase(IsLive ? "live" : "debug");
            }
        }
    }

