﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Data;
using System.Configuration;
using TaskReminders.Properties;
using Cartus.Mailing;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
namespace TaskReminders
{
    class Program
    {
        //application settings
        //test comment 
        readonly static string applicationName = ConfigurationManager.AppSettings["applicationName"];
        readonly static string debugEmailSuffix = ConfigurationManager.AppSettings["debugEmailSuffix"];

        static int maxTasks = 1;
        static bool isLive = false;
        static string conn = string.Empty;
        static string url = string.Empty;
        static object padlock = new object();
        //set up logging
        static string startPath = AppDomain.CurrentDomain.BaseDirectory;
        static EventLogTraceListener eventLogListener = new EventLogTraceListener(applicationName);
        static TextWriterTraceListener logListener = new TextWriterTraceListener($"{startPath}\\log{DateTime.Now.ToString("yyyyMMddHHmmss")}.txt");
        static void Main(string[] args)
        {
            try
            {
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
                Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-GB");
                //adjust settings
                bool.TryParse(ConfigurationManager.AppSettings["isLive"], out isLive);
                conn = ConfigurationManager.ConnectionStrings[isLive ? "live" : "debug"].ToString();
                int.TryParse(ConfigurationManager.AppSettings["maxTasks"], out maxTasks);
                url = ConfigurationManager.AppSettings[isLive ? "url" : "debugUrl"].ToString();
                WriteEventLogEntry("Task Reminders check started");
                WriteLogEntry("Task Reminders check started");
                //---------------------------------------------
                // regular task list notifications
                //---------------------------------------------
                //get data
                List<Reminder> Reminders = new ReminderData().GetReminders();
                if (Reminders.Count > 0)
                {
                    string template = Resources.tasks;
                    //get users
                    List<String> users = Reminders.Select(c => c.UserName).Distinct().ToList();
                    //distinct user loop
                    Parallel.ForEach(users, user =>
                    {
                        string message = template;
                        StringBuilder taskList = new StringBuilder();
                        string taskHeader = "My Task List";
                        List<Reminder> userFilter = Reminders.FindAll(c => c.UserName.Equals(user)).OrderBy(c => c.Action_Date).ToList();
                        List<int?> branches = userFilter.Select(c => c.Supplier_No).Distinct().ToList();
                        //distinct branch loop
                        branches.ForEach(branch =>
                        {
                            List<Reminder> branchFilter = userFilter.FindAll(c => c.Supplier_No.Equals(branch)).OrderByDescending(c => c.Action_Date).ToList();
                            int count = branchFilter.Count;
                            int remaining = count - maxTasks;
                            taskList.Append($"<p class=\"bold\">{branchFilter[0].Supplier_Name}</p>");
                            taskList.Append("<ul class=\"highlight\">");
                            if (count > maxTasks)
                            {
                                for (int i = 0; i < 4; i++)
                                {
                                    taskList.Append(BuildTaskElement(branchFilter[i]));
                                }
                                taskList.Append("</ul>");
                                taskList.Append($"<p>...and {remaining.ToString()} other tasks</p>");
                            }
                            else
                            {
                                for (int i = 0; i < branchFilter.Count; i++)
                                {
                                    taskList.Append(BuildTaskElement(branchFilter[i]));
                                }
                                taskList.Append("</ul>");
                            }
                        });
                        if (userFilter[0].branch_in == true)
                        {
                            taskHeader = "Branch Task List";
                        }
                        message = message.Replace("***FirstName***", userFilter[0].Firstname);
                        message = message.Replace("***Tasks***", taskList.ToString());
                        message = message.Replace("***Link***", $"<a href=\"{url}loginform.aspx?user={user}\">login</a>");
                        message = message.Replace("***TasksHeader***", taskHeader);
                        using (MailerEmail msg = new MailerEmail())
                        {
                            if (isLive)
                            {
                                msg.ADDRESSES.Add(new MailerAddress() { ADDRESS = userFilter[0].CorrespondenceEmail, TYPE = AddressType.TO });
                                msg.ADDRESSES.Add(new MailerAddress() { ADDRESS = "webmasteruk@cartus.com", TYPE = AddressType.BCC });
                                msg.SUBJECT = "Relocation Agent Network - Task Reminder";
                            }
                            else
                            {
                                msg.ADDRESSES.Add(new MailerAddress() { ADDRESS = "webmasteruk@cartus.com", TYPE = AddressType.TO });
                                msg.SUBJECT = $"Relocation Agent Network - Task Reminder {debugEmailSuffix}";
                            }
                            msg.ADDRESSES.Add(new MailerAddress() { ADDRESS = "relocationagentsupport@cartus.com", TYPE = AddressType.FROM });
                            msg.APPLICATION = applicationName;
                            msg.HTML = true;
                            msg.BODY = message;
                            msg.Queue(conn);
                        }
                        lock (padlock)
                        {
                            WriteLogEntry($"Queued Email to {userFilter[0].CorrespondenceEmail}");
                        }
                    });
                }
                //---------------------------------------------
                // referral status task notification
                //---------------------------------------------
                List<StatusReminder> StatusReminders = new ReminderData().GetStatusReminders();
                if (StatusReminders.Count > 0)
                {
                    string template = Resources.statustasks;
                    Parallel.ForEach(StatusReminders, Reminder =>
                    {
                        string message = template;
                        message = message.Replace("***FirstName***", Reminder.Firstname);
                        if (Reminder.UserName != null)
                        {
                            message = message.Replace("***Link***", $"<a href=\"{url}loginform.aspx?user={Reminder.UserName}\">login</a>");
                        }
                        else
                        {
                            message = message.Replace("***Link***", $"<a href=\"{url}loginform.aspx\">login</a>");
                        }
                        message = message.Replace("***StatusTask***", BuildTaskElement(Reminder));
                        bool queued = false;
                        using (MailerEmail msg = new MailerEmail())
                        {
                            if (isLive)
                            {
                                msg.ADDRESSES.Add(new MailerAddress() { ADDRESS = Reminder.CorrespondenceEmail, TYPE = AddressType.TO });
                                msg.ADDRESSES.Add(new MailerAddress() { ADDRESS = "webmasteruk@cartus.com", TYPE = AddressType.BCC });
                                msg.SUBJECT = "Relocation Agent Network - Status Task Reminder";
                            }
                            else
                            {
                                msg.ADDRESSES.Add(new MailerAddress() { ADDRESS = "webmasteruk@cartus.com", TYPE = AddressType.TO });
                                msg.SUBJECT = $"Relocation Agent Network - Status Task Reminder {debugEmailSuffix}";
                            }
                            msg.ADDRESSES.Add(new MailerAddress() { ADDRESS = "relocationagentsupport@cartus.com", TYPE = AddressType.FROM });
                            msg.APPLICATION = applicationName;
                            msg.HTML = true;
                            msg.BODY = message;
                            queued = msg.Queue(conn);
                        }
                        if (queued)
                        {
                            //flag the status reminder record as complete
                            new ReminderData().UpdateReminder(new ReminderUpdate() { Id = Reminder.Id });
                        }
                    });
                }
                WriteEventLogEntry("Task Reminders check finished");
                WriteLogEntry("Task Reminders check finished");
            }
            catch (Exception ex)
            {
                lock (padlock)
                {
                    WriteEventLogEntry(ex.Message);
                    WriteLogEntry(ex.Message);
                }
            }
        }
        static string BuildTaskElement(dynamic reminder)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-GB");
            try
            {
                return $@"<li>In respect of {
                reminder.Referee_Other_Names} {
                reminder.Referee_Surname} of {
                reminder.Referee_Address1}, {
                reminder.Referee_Post_Outcode} {
                reminder.Referee_Post_Incode}<br />{
                reminder.Task_Description}<br /><b>Action Due: {(
                reminder.Action_Date.TimeOfDay.Ticks == 0 ?
                reminder.Action_Date.ToString("dd/MM/yyyy") :
                reminder.Action_Date.ToString("dd/MM/yyyy HH:mm"))}</b></li>";
            }
            catch (Exception ex)
            {
                lock (padlock)
                {
                    WriteEventLogEntry(ex.Message);
                    WriteLogEntry(ex.Message);
                }
                return String.Empty;
            }
        }
        static void WriteEventLogEntry(string message)
        {
            eventLogListener.WriteLine($"{DateTime.Now.ToString("dd/MM/yy HH:mm:ss")} - {message}");
            eventLogListener.Flush();
        }
        static void WriteLogEntry(string message)
        {
            logListener.WriteLine($"{DateTime.Now.ToString("dd/MM/yy HH:mm:ss")} - {message}");
            logListener.Flush();
        }
    }
}
